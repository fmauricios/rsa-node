'use strict'

const fs = require('fs')
const chalk = require('chalk')

module.exports = function write (fileName, privateKey, publicKey) {
  let data = `publicKey:\n${publicKey}\nprivateKey:\n${privateKey}`

  fs.open(`./keys/llaves${fileName}`, 'r', function (err, fd) {
    if (err) {
      fs.writeFile(`./keys/llaves${fileName}`, data, function (err) {
        if (err) return console.log(`${chalk.red('Ups! An error was detected writing this file')} ${err}`)

        console.log(`${chalk.green('The file was saved, please see the keys folder.')}`)
      })

      fs.writeFile(`./files/archivo${fileName}`, publicKey, function (err) {
        if (err) return console.log(`${chalk.red('Ups! An error was detected writing this file')} ${err}`)

        console.log(`${chalk.green('The input was saved, please see the files folder.')}`)
      })
    } else {
      fs.writeFileSync(`./keys/llaves${fileName}`, data)
      fs.writeFileSync(`./files/archivo${fileName}`, publicKey)

      console.log(`${chalk.green('The file was overwrite, please see the keys folder.')}`)
    }
  })
}
