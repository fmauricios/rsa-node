'use strict'

const chalk = require('chalk')
const fs = require('fs')

const { decrypt } = require('./rsa-creation')

module.exports = function read (fileName) {
  fs.readFile(`./keys/llaves${fileName}`, function (err, data) {
    if (err) return console.log(`${chalk.red('No files found for selected user.')}`)

    const encrypted = data.toString().split('\n')[3]
    const decrypted = decrypt(encrypted)

    if (decrypted) {
      console.log(`${chalk.green(decrypted)}`)
    }
  })
}
