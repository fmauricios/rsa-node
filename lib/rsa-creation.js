'use strict'

const NodeRSA = require('node-rsa')

const key = new NodeRSA({ b: 1024 })
key.setOptions({encryptionScheme: 'pkcs1'})
key.generateKeyPair(1024)

function encrypt (text) {
  const encrypted = key.encrypt(text, 'base64', 'utf-8')

  return {
    encrypted,
    key
  }
}

function decrypt (encrypted) {
  try {
    return key.decrypt(encrypted, 'utf8')
  } catch (e) {
    return console.log('It was not possible to decrypt this file.')
  }
}

module.exports = {
  encrypt,
  decrypt
}
