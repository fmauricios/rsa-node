'use strict'

const chalk = require('chalk')
const inquirer = require('inquirer')

const { encrypt } = require('./lib/rsa-creation')
const write = require('./lib/rsa-writing')
const read = require('./lib/rsa-reading')

const prompt = inquirer.createPromptModule()

async function run () {
  const answer = await prompt([
    {
      type: 'list',
      name: 'user',
      choices: [
        'A',
        'B',
        'C'
      ],
      message: 'Choose an user.'
    }
  ])

  const answer2 = await prompt([
    {
      type: 'list',
      name: 'operation',
      choices: [
        'Write',
        'Read'
      ],
      message: 'Choose an operation.'
    }
  ])

  if (answer2.operation === 'Write') {
    const input = await prompt([
      {
        type: 'input',
        name: 'text',
        message: 'Enter a string of characters to encrypt.'
      }
    ])

    if (!input.text) {
      return console.log(`${chalk.red('Run it again, you should enter at least one character.')}`)
    }

    if (input.text.length > 300) {
      return console.log(`${chalk.red('Run it again, you should enter a maximum of 300 characters.')}`)
    }

    const encrypted = encrypt(input.text)

    let privateKey = encrypted.encrypted
    let publicKey = encrypted.key['$cache']['pkcs8-public-pem'].split('\n')
    publicKey.shift()
    publicKey.splice(-1, 1)
    publicKey = publicKey.join('')

    write(answer.user, privateKey, publicKey)
  } else {
    read(answer.user)
  }
}

run()
